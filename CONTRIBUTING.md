## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._

---

## Contribute to Step Runner

Thank you for your interest in contributing to GitLab. For more information on the contribution process see the
[why contribute to GitLab](https://docs.gitlab.com/ee/development/contributing/#contribute-to-gitlab) and 
[how to contribute to GitLab](https://about.gitlab.com/community/contribute/) pages.

The following content is an extension of the [GitLab contribution guidelines](https://docs.gitlab.com/ce/development/contributing/index.html).

## Project License

You can view this projects license in [LICENSE.md](LICENSE).

## Issues

To get support for your particular problem please use the [getting help channels](https://about.gitlab.com/getting-help/).

The [Step Runner issue tracker on GitLab.com][steprunner-tracker] is the right place for bugs and feature proposals about the GitLab Step Runner.
Please use the ~"section::ci", ~"devops::verify", and ~"group::runner" labels when opening a new issue to ensure it is
quickly reviewed by the right people.

[Search the issue tracker][steprunner-tracker] for similar entries before
submitting your own, there's a good chance somebody else had the same issue or
feature proposal. Show your support with an award emoji and/or join the
discussion.

Not all issues will be addressed and your issue is more likely to
be addressed if you submit a merge request which partially or fully solves
the issue. If it happens that you know the solution to an existing bug, please first
open the issue in order to keep track of it and then open the relevant merge
request that potentially fixes it.

[steprunner-tracker]: https://gitlab.com/gitlab-org/step-runner/-/issues

## Contributer Code of conduct

We want to create a welcoming environment for everyone who is interested in contributing.
Visit our [Code of Conduct page](https://about.gitlab.com/community/contribute/code-of-conduct/) to read our community pledge and standards.
